#!/usr/bin/python3

from variables import var
from nmap_scanning import *


class WebServices:
        '''
        This will check to see if there are any web related services being run, 
        if any are found then its able to start further enumeration on each port.
        '''


        def InitialWebCheck(self):
                '''
                Primary purpose is to check which tests to be performed based on available ports found.

                '''

                # Comparing common port numbers to initial NMAP scan, made global for now temporary fix
                global web_port

                for web_port in nmap_scan.port_open():

                        if web_port in var.HTTP:
                                var.URL = "http://"
                                self.host_web(var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD)
                                self.directory_scan(var.MSG, var.MSG2,var.URL, var.SERVICE, var.SERVICE_CMD)

                        if web_port in var.HTTPS:
                                var.URL = "https://"
                                self.host_web(var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD)
                                self.directory_scan(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)

                        if web_port in var.DNS:
                                self.dns(var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD)

                        else:

                                continue



        def host_web(self, MSG, MSG2, SERVICE, SERVICE_CMD):
                '''
                Check to see if any host information is resolvable based on running web service.
                '''

                var.MSG = ""

                var.SERVICE = f"HOSTNAME / IP CHECK TCP: {web_port}"
                var.SERVICE_CMD = f"host -a {var.TARGET}"

                var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)

                return var.MSG, MSG2, var.SERVICE, var.SERVICE_CMD



        def dns(self, MSG, MSG2, SERVICE, SERVICE_CMD):
                '''
                Basic tests against any DNS service (TCP:53) found based on initial NMAP scan.
                '''

                var.MSG = ""

                var.SERVICE = f"DNS INFO TCP: {web_port}"
                var.SERVICE_CMD = f"dnsenum -v {var.TARGET}"

                var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)

                return var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD


        def directory_scan(self, MSG, MSG2, URL, SERVICE, SERVICE_CMD):
                '''
                If any possible web related ports found, then one of the most basic checks needed is to run
                directory enumeration in the background. Simple enumeration will be started and the user can 
                then decide if further enum is needed.
                If both 80 & 443 found, will prompt user which service to fuzz (Future TODO)
                MSG: Text line just to use the same one across similar services
                URL: Prepended for either http/s services
                To install sec-lists manually: apt -y install seclists 
                '''

                # Chose to use seclists, short list less than 100 entries
                # If you dont want seclists then you can edit the wordlist to one you prefer
                wordlist = "https://raw.githubusercontent.com/danielmiessler/SecLists/master/Discovery/Web-Content/Logins.fuzz.txt"
                downloaded_wordlist = "logins.fuzz.txt"

                var.SERVICE = f"DIRECTORY SCAN ON TCP: {web_port}"
                var.MSG = "\n[+] Grabbing Seclist: logins.fuzz.txt"
                # Removed -m flag seems no longer needed in gobuster
                var.SERVICE_CMD = f"wget -q {wordlist} -O logins.fuzz.txt;\ngobuster dir -w {downloaded_wordlist} -u {var.URL}{var.TARGET}"

                var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)

                return var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD


web_scan = WebServices()
