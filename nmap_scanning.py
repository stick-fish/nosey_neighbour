#!/usr/bin/python3

from variables import var
from time import sleep
import nmap
import sys


class Nmap:

        '''
        Initial nmap scan against target, outputs results in all formats depending which you prefer to use. 
        The start of the program is here, based on what the NMAP module is able to find will determine the rest of the checks.
        Should the NMAP scan take too long or you know the ports beforehand, edit the 'host' and 'ports' variables below.
        Built and tested on nmap version python-nmap 0.6.4 library (https://pypi.org/project/python-nmap/)   
        '''

        def InitialScan(self):
                '''
                The start of the program is here, based on what the NMAP module is able to find will determine the rest of the checks.
                Should the NMAP scan take too long or you know the ports beforehand, edit the 'host' and 'ports' variables below.
                '''

                # Check the current status of the target system.
                status = "open"
                host = var.TARGET
                ports =  var.NMAP_PORTS
                scan_args = var.FLAGS

                try:

                        var.cmd(var.GREEN)
                        print(f"[+] Running NMAP initial scan on {host} ports: {ports} ...")
                        var.cmd(var.PLAIN)

                        # NMAP Scan starts here
                        scanner.scan(host, ports, arguments=scan_args)

                        # Prints command used by NMAP library
                        print(f"[!] Command used: {scanner.command_line()}\n")

                        print(var.HORIZ)
                        var.cmd(var.GREEN)
                        print(f"[+] TARGET IP / (HOSTNAME) : {host} / ({scanner[host].hostname()})")

                        # TODO: Get better OS detection without sudo, for now based on port info
                        #print(f"[+] TARGET OS (Best guess) : {OS}")


                        if scanner[host].state() == "down":

                                # If the host is down
                                var.cmd(var.RED)
                                print(f"[-] CURRENT STATUS : {scanner[host].state().upper()}")
                                print(f"[-] Seems {var.TARGET} may be down or incorrect IP used.")

                                sys.exit()


                        else:

                                # If the host is up
                                print(f"[+] CURRENT STATUS : {scanner[host].state().upper()}")

                                # Check if TCP or UDP in use, depends on NMAP flags
                                for proto in scanner[host].all_protocols():

                                        found_ports = scanner[host][proto].keys()
                                        print(f"[+] PROTOCOL : {proto.upper()}")

                                print(f"[+] PORTS FOUND : {len(found_ports)}")

                                var.cmd(var.YELLOW)

                                # List the ports found with status, well known service name, version info if any
                                # If no version info found the field will be blank

                                # Heading for port outputs
                                print(f"PORT\t\tSTATUS\t\tSERVICE\t\t\tVERSION\n{var.HORIZ}")

                                global port_list
                                global exdb_list

                                exdb_list = {"port":[], "product":[], "version":[]};
                                port_list = []

                                for port in found_ports:

                                        # Shortend this to avoid overflow on page
                                        host_attr = scanner[host][proto][port]

                                        print(f" {port}\t \t{host_attr['state']} \t\t{host_attr['name']} \t\t\t{host_attr['product']} {host_attr['version']} {host_attr['extrainfo']}")

                                        port_list.append(port)

                                        exdb_list["port"].append(port)
                                        exdb_list["product"].append(host_attr['product'])
                                        exdb_list["version"].append(host_attr['version'])


                        var.cmd(var.PLAIN)

                except:

                        var.cmd(cmd.RED)
                        print(f"[-] Seems {var.TARGET} may be down or incorrect IP/Port format used.")

                        sys.exit()


        def port_open(self):

                return port_list


        def active_services_name(self):

                product = exdb_list["product"]

                return product


        def active_services_version(self):

                version = exdb_list["version"]

                return version


nmap_scan = Nmap()
scanner = nmap.PortScanner()


'''
        def os_guess(self):

                # Guesses OS based on TTL value, just to avoid having to use sudo
                # Possible future use
                LINUX = 64
                WINDOWS = 128
                Solaris_AIX = 254 
'''