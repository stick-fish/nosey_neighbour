#!/usr/bin/python3

import os
import sys


class Vars:

        '''
        General class for all shared variables or functions etc
        This page will become the main area for setting preferences eg NMAP flags.
        Will start moving the repeated items here in future updates.
        '''

        NOSEY_VER = "2.3"


        BANNER = f'''
          _   _                        _   _      _       _     _                       
         | \ | | ___  ___  ___ _   _  | \ | | ___(_) __ _| |__ | |__   ___  _   _ _ __  
         |  \| |/ _ \/ __|/ _ \ | | | |  \| |/ _ \ |/ _` | '_ \| '_ \ / _ \| | | | '__| 
         | |\  | (_) \__ \  __/ |_| | | |\  |  __/ | (_| | | | | |_) | (_) | |_| | |    
         |_| \_|\___/|___/\___|\__, | |_| \_|\___|_|\__, |_| |_|_.__/ \___/ \__,_|_|    
                               |___/                |___/                               

        stickfish V{NOSEY_VER} 2024
        ><> 
        Initial Target Enumeration 
        '''


        # Target system ( RHOST )
        TARGET = str(sys.argv[1])


        # Terminal output colors, kept as class variables for ease
        RED = 'echo "\e[31m"'
        GREEN = 'echo "\e[32m"'
        YELLOW = 'echo "\e[33m"'
        BLUE = 'echo "\e[34m"'
        PLAIN = 'echo "\e[0m"'
        BOLD = 'echo "\e[1m"'


        # Horizontal line seperator for each section
        HORIZ = '---' * 50 + '(0)'

        ######### NMAP Options for initial scan ######### 

        #NMAP_PORTS = "1-65535"
        NMAP_PORTS = "1-1000"

        # Best flag combo here just in case you fiddled too much and want the original settings back: -A -T4 --open --osscan-guess -vvv
        # Removed --osscan-gues for now
        FLAGS = f"-A -T4 --open -v"

        # Not used currently, but if you choose to create one add to the FLAGS section -oA {OUTPUT_FILE}
        OUTPUT_FILE = "/tmp/nmap-initial.txt"

        ######### END of NMAP Options #########


        # Port lists to be used across all scripts
        # Created lists here should extra ports be added that are not running on well known numbering

        FTP = [21]
        RPC = [111]
        SMB = [139, 445]
        NFS = [2049]
        HTTP = [80, 8080, 8081, 8180]
        HTTPS = [443]
        DNS = [53]


        # Constants used in the template for all ports found
        MSG = ""
        MSG2 = "" 
        URL = ""
        SERVICE = ""
        SERVICE_CMD = ""


        def template(self, MSG, MSG2, URL, SERVICE, SERVICE_CMD):
                '''
                Minimal template to keep the web service related info uniform.
                Each web method can now use the same template, but only need to change 2 variables.
                SERVICE & SERVICE_CMD will be the constant across all methods here and only set them 
                for each specific task / check.
                '''

                # TODO: Find a better way to apply coloring to avoid all the extra new lines added
                print(var.HORIZ, end = '\n')
                var.cmd(var.GREEN + ';' + var.BOLD)

                print(f"[+] {SERVICE}", end = '')
                print(f"\n{MSG}", end = '')

                var.cmd(var.PLAIN)
                print(f"[!] Command used:\n{SERVICE_CMD}", end = '')
                var.cmd(var.YELLOW)
                print(f"{MSG2}")

                var.cmd(SERVICE_CMD)
                var.cmd(var.PLAIN)
                URL = ""


        # Simple command function to run stuff in terminal
        def cmd(self, command):
                command = os.system(command)
                return command


var = Vars()


'''
# Potential future use.
# Reading file contents to check against ( Left here just in case needed )
def read_file(self):
        with open(self.OUTPUT_FILE) as file:
                content = file.readlines()
                for line in content:
                        print(line)


# Socket kept for possible future use
def connect_service(self, CMD):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((var.TARGET, file_port))
        s.send(CMD)
        s.recv(1024)

'''
