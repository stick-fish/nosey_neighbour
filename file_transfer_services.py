#!/usr/bin/python3

from variables import var
from nmap_scanning import *
from ftplib import FTP


class FileTransfer:
	'''
	This class tests all services related to file transferring services that may be open.
	E.g: FTP, NFS, SMB etc
	The script will attempt to access the service using default or anonymous user logins.
	Have added in basic enumeration here as well to help group similar items. 
	'''

	
	def InitialFileCheck(self):
		
		# Comparing common port numbers to initial NMAP scan, made global for now temporary fix
		global file_port

		for file_port in nmap_scan.port_open():

			if file_port in var.FTP:				
				self.ftp(var.MSG, var.MSG2, var.SERVICE)												
			
			if file_port in var.SMB:
				self.smb(var.MSG, var.SERVICE, var.SERVICE_CMD)								
			
			if file_port in var.NFS:
				self.nfs(var.MSG, var.SERVICE, var.SERVICE_CMD)

			if file_port in var.RPC:
				self.rpc(var.MSG, var.SERVICE, var.SERVICE_CMD)
			
			else:

				continue


	def ftp(self, MSG, MSG2, SERVICE):
		'''
		Check to see if anonymous login is allowed for FTP, get the version if able to.
		'''

		# Defaults to use anonymous:anonymous
		ftp = FTP(var.TARGET)
		ftp_banner = ftp.getwelcome()

		var.SERVICE = f"FTP SERVICE TCP: {file_port}\n[?] Attempting FTP login using 'ftp {var.TARGET} {file_port}' anonymous:anonymous..."		
		
		if ftp_banner:

			# Ignoring this service command as the FTP lib already executes it
			var.SERVICE_CMD = ""
			var.MSG = ""

			var.MSG2 = f"[+] Successful Login!\n {ftp.getwelcome()}"
			ftp.quit()

		else:

			ftp.quit()
			var.RED
			var.MSG = "[-] Unable to login.\n"
		
		var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)
		
		return var.MSG, var.MSG2, var.SERVICE


	def rpc(self, MSG, SERVICE, SERVICE_CMD):
		'''
		Checking RCPINFO to find any useful details.
		'''

		var.MSG = ""
		var.MSG2 = ""

		var.SERVICE = f"RPC INFO TCP: {file_port}"
		var.SERVICE_CMD = f"rpcinfo -p {var.TARGET}"

		var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)
		
		return var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD

	
	def smb(self, MSG, SERVICE, SERVICE_CMD):
		'''
		Check SMB service to try find any available shares.
		'''

		var.MSG = ""
		var.MSG2 = ""

		var.SERVICE = f"SMB INFO TCP: {file_port}"
		var.SERVICE_CMD = f"smbmap -H {var.TARGET} -R -P {file_port}"

		var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)
		
		return var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD


	def nfs(self, MSG, SERVICE, SERVICE_CMD):
		'''
		Check for any available exported shares on target.
		'''
		
		var.MSG = ""
		var.MSG2 = ""

		var.SERVICE = f"NFS INFO TCP: {file_port}"
		var.SERVICE_CMD = f"showmount -e {var.TARGET}"

		var.template(var.MSG, var.MSG2, var.URL, var.SERVICE, var.SERVICE_CMD)
		
		return var.MSG, var.MSG2, var.SERVICE, var.SERVICE_CMD


file_scan = FileTransfer()
