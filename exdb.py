#!/usr/bin/python3

import os
import sys
from variables import var
from web_services import *
from file_transfer_services import *
from nmap_scanning import *


class Exploit:
	'''
	This will check against the found services and if any version information is returned via NMAP,
	that will then be used to suggest possible exploits matched to the search term given. 
	NOTE: The results may be false or a long list, it's your responsibility to double check the results.
	'''
	

	def search_database(self):
		'''
		Gather service info into single point and pass to searchsploit to compare information.
		It will use the service name and any version info to search against local searchsploit installation.
		To update searchsploit: searchsploit -u

		'''

		# Kept the formatting in here due to simplicity checking the services info (Will update and seperate formatting into a template)
		print(var.HORIZ)
		
		var.cmd(var.GREEN + ';' + var.BOLD)
		print("[+] Checking Exploit Database for any matches...")
		print("[!!] Only checking against services with found versions!")
		
		var.cmd(var.PLAIN)
		var.cmd(var.BOLD)

		exdb_header = print("PORT NO\t\tSERVICE RUNNING\t\tVERSION FOUND\n")
		
		var.cmd(var.PLAIN)
		
		i = 0
		open_port_len = len(nmap_scan.port_open())


		for i in range(i, open_port_len):
			
			service_running = nmap_scan.active_services_name()[i]
			service_info = nmap_scan.active_services_version()[i]
			port_num = nmap_scan.port_open()[i]
			
			print(exdb_header)
			
			var.cmd(var.BLUE)
			print(var.HORIZ)
			
			var.cmd(var.GREEN + ';' + var.BOLD)
			print(f"[+] PORT: {port_num}\t{service_running}\t\t\t {service_info}")

			var.cmd(var.PLAIN)

			# Command use to check against searchsploit DB
			search_cmd = f"searchsploit {service_running} {service_info} --exclude='/dos/|/local/' | head -20 2>/dev/null"

			empty_result = ["", " ", None, "None", "0", 0]

			if f"{service_running}" in empty_result:

				pass

			else:

				print(f"[+] {search_cmd}")
				
				var.cmd(var.YELLOW)
				var.cmd(search_cmd)

				var.cmd(var.PLAIN)

			
			i += 1

			


exdb = Exploit()

'''
elif f"{service_info}" in empty_result:

				continue
'''